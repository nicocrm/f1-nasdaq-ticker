# Overview 

Use GCS-WEB API to provide a ticker box.

 - https://clientapi.gcs-web.com/data/364c841b-f9ec-4d7f-8212-3aee03452809/Quotes
 - iterate and get only the first record from `data` with `isIndex` = "false"
 - display: `bid`, `symbol`, `changeNumber`, `date`, `exchange`

# Development

Run `composer install` to install dependencies.

# Usage

The plugin provides a shortcode that can be used to include the ticker data:

```
[nasdaq_ticker accessid="xxxxxxxxxxxxxxxxxx"]
```

The specified accessid will be used to retrieve ticker data and display it in a div
(the theme will need to be tailored to display this div appropriately)


It's also possible to configure a "Footer Accessid" in the plugin settings, if that is set
the ticker will be included from the footer action.

Example ticker style:


```

#page {
    position: relative;
}

.nasdaq-ticker {
    position: absolute;
    background-color: #0F75BC;
	line-height: 150%;
	text-align: center;
    padding: 2px 10px 0 10px;
    top: 188px;
    left: 30px;
    z-index: 15;
    color: white;
    font-weight: normal;
    font-family: "proxima-nova","Helvetica Neue",Helvetica,Arial,sans-serif;
	font-size: 16px;
}

.nasdaq-ticker .exchange {
}
.nasdaq-ticker .quote {
	font-size: 24px;
}
.nasdaq-ticker .change {
}
.nasdaq-ticker .change img {
	vertical-align: middle;
}
.nasdaq-ticker .date {
    font-style: italic;
	font-size: 12px;
}
```
