<?php
/**
 * Plugin Name: NASDAQ Ticker
 * Description: Use GCS-WEB API to provide a ticker box.
 * Version: 1.0
 * Author: f1code
 * Author URI: http://f1code.com/
 * License: ISC
 *
 * Copyright (C) 2018 f1code
 */

//spl_autoload_register(function ($class) {
//    // project-specific namespace prefix
//    $prefix = 'AWC\\SocialPledge\\';
//
//    // base directory for the namespace prefix
//    $base_dir = __DIR__ . '/src/';
//
//    // does the class use the namespace prefix?
//    $len = strlen($prefix);
//    if (strncmp($prefix, $class, $len) !== 0) {
//        // no, move to the next registered autoloader
//        return;
//    }
//
//    // get the relative class name
//    $relative_class = substr($class, $len);
//
//    // replace the namespace prefix with the base directory, replace namespace
//    // separators with directory separators in the relative class name, append
//    // with .php
//    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';
//
//    // if the file exists, require it
//    if (file_exists($file)) {
//        require $file;
//    }
//});

require_once 'vendor/autoload.php';

define('NASDAQ_TICKER_PLUGIN', __FILE__);
define('NASDAQ_TICKER_PLUGIN_BASENAME', plugin_basename(NASDAQ_TICKER_PLUGIN));

add_action('init', [new \F1\NasdaqTicker\Init(), 'initialize']);
add_action('wp_loaded', [new \F1\NasdaqTicker\Init(), 'onWpLoaded']);
