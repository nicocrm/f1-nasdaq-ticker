<?php
/**
 * Init.php
 * Created By: nico
 * Created On: 07/07/2018
 */

namespace F1\NasdaqTicker;

/**
 * Top level initialization
 *
 * @package F1\NasdaqTicker
 */
class Init
{
    /**
     * Called on init - this will bootstrap the rest of the plugin.
     */
    function initialize()
    {
    }


    /**
     * Called on wp_loaded - for functions that depend on WP being loaded
     */
    function onWpLoaded()
    {
        (new ShortcodeDef())->registerShortCode();
        (new OptionPage())->registerOptionPage();
        $accessid = OptionPage::getTickerOption(OptionPage::OPTION_FOOTER_ACCESSID);
        // if($accessid && $_SERVER['REQUEST_URI'] == '/') {
        if($accessid) {
            add_action('get_footer', [new Ticker($accessid), 'render']);
        }
    }
}
