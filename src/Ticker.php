<?php

namespace F1\NasdaqTicker;

class Ticker {
    function __construct($accessid) {
        $this->accessid = $accessid;
    }

    // get the data from cache or API
    // return ticker HTML content
    function getHtml() {
        $data = $this->getData();
        if($data) {
			// $changeImage = plugins_url('assets/' . ($data->changeNumber < 0 ? 'down' : 'up') . '_arrow.png', NASDAQ_TICKER_PLUGIN);
            $change = $data->changeNumber < 0 ? 'down' : 'up';
            $html = "<div class='nasdaq-ticker'>
                <div class='exchange'>$data->exchange</div>
                <div class='quote'>
                    <span class='symbol'>$data->symbol</span>
                    <span class='bid'>$".number_format($data->bid, 2)."</span>
                </div>
                <div class='change'>
                    <i class='fa fa-caret-$change'></i>
                    $ $data->changeNumber
                </div>
                <div class='date'>
                    As of: 
                        <time datetime='$data->date'>" . 
                        date('n/j/Y h:i a', strtotime($data->date)) . 
                        "</time>
                </div>
            </div>";
            return $html;
        }
        return '';
    }

    function render() {
        print($this->getHtml());
    }

    private function getData() {
        $cached = wp_cache_get('nasdaq_ticker_' . $this->accessid);
        if($cached)
            return $cached;
        $url = "https://clientapi.gcs-web.com/data/$this->accessid/Quotes";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        if($result) {
            $json = json_decode($result);
        }
    //     $result = json_decode('
    //     {
    // "data": [
    //     {
    //         "52WeekHigh": 76.68,
    //         "52WeekHighDate": "2018-06-07T00:00:00",
    //         "52WeekLow": 63.23,
    //         "52WeekLowDate": "2017-08-03T00:00:00",
    //         "askSize": 1,
    //         "bid": 73.09,
    //         "bidSize": 1,
    //         "changeNumber": 0.4,
    //         "changePercent": 0.55,
    //         "class": "Common",
    //         "date": "2018-07-06T16:03:00",
    //         "dayHigh": 73.59,
    //         "dayLow": 72.53,
    //         "exchange": "NYSE Arca",
    //         "exchangeShortCode": "NYS A",
    //         "firstTradeDate": "2010-01-01T00:00:00",
    //         "isDefault": "true",
    //         "isIndex": "false",
    //         "lastTrade": 73.1,
    //         "marketCap": 42471100000,
    //         "open": 72.75,
    //         "peRatio": 25.0891,
    //         "previousClose": 72.7,
    //         "rollingEPS": 2.8977,
    //         "securityId": 4610,
    //         "sharesOutstanding": 581000000,
    //         "symbol": "ICE",
    //         "volume": 564580,
    //         "yield": 1.3205
    //     } ] }
    // ');
        if($json) {
            foreach($json->data as $point) {
                if($point->isIndex == "false") {
                    wp_cache_set('nasdaq_ticker_' . $this->accessid, $point, '', 60 * 1);   // every minute
                    return $point;
                }
            }
        }
        return null;
    }
}
