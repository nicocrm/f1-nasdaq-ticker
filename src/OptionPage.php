<?php
/**
 * OptionPa.php
 * Created By: nico
 * Created On: 11/28/2015
 */

namespace F1\NasdaqTicker;

use F1\WPUtils\Admin\AdminPageHelper;

class OptionPage extends AdminPageHelper
{
    const F1_NASDAQTICKER_SETTINGS = 'f1_nasdaqticker_settings';
    const OPTION_FOOTER_ACCESSID = 'footer_accessid';

    function __construct()
    {
        parent::__construct(self::F1_NASDAQTICKER_SETTINGS, 'NASDAQ Ticker Settings');
        $this->addSetting(self::OPTION_FOOTER_ACCESSID, 'Footer Ticker Access ID');
    }

    public function registerOptionPage()
    {
        parent::registerOptionPage(NASDAQ_TICKER_PLUGIN_BASENAME);
    }

    public static function getTickerOption($optionName)
    {
        $opts = new OptionPage();
        return $opts->getOption($optionName);
    }
}
