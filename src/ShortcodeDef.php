<?php

namespace F1\NasdaqTicker;

/**
 * Shortcode definition for "NasdaqTicker" 
 *
 * @package F1\NasdaqTicker
 */
class ShortcodeDef
{
    public function registerShortCode()
    {
        add_shortcode('nasdaq_ticker', [$this, 'renderTicker']);
    }

    public function renderTicker($atts)
    {
        $atts = shortcode_atts(['accessid' => ''], $atts);

        if($atts['accessid']) {
            return (new Ticker($atts['accessid']))->getHtml();
        }

        return '';
    }
}
